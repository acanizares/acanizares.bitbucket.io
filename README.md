# README #

## What is this?

A personal website that links to selected math and programming projects.

**Note:** 
Some links might not be active, meaning that the project is not finished yet.
If you would like to have access to those please contact me via

`ana DOT canizares DOT g AT gmail DOT com`

The code is based on 
[Craig Anthony's Flexbox Card Grid](https://codepen.io/mcraiganthony/pen/NxGxqm?depth=everything&order=popularity&page=2&q=cards&show_forks=false)
and [Scott Zirkel's HRs](https://codepen.io/scottzirkel/pen/yNxNME). The website is hosted on Bitbucket Cloud at [acanizares.bitbucket.io](https://acanizares.bitbucket.io).